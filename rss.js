
jQuery.noConflict();

jQuery(function() {
    jQuery('div.rssFeed').each(function(){

    // 新しいページを開かない
    jQuery(this).children('a').attr("target", "_self");
    });
});

jQuery(function() {
    jQuery('div.rssFeed').each(function(){
    // 曜日と月の読み替えリスト
        var week = {
            "月" : /Mon/, "火" : /Tue/, "水" : /Wed/,
            "木" : /Thu/, "金" : /Fri/, "土" : /Sat/, "日" : /Sun/ };
        var month = {
            "1月" : /Jan/,"2月" : /Feb/,"3月" : /Mar/,
            "4月" : /Apr/,"5月" : /May/,"6月" : /Jun/,
            "7月" : /Jul/,"8月" : /Aug/,"9月" : /Sep/,
            "10月" : /Oct/,"11月" : /Nov/,"12月" : /Dec/ };

        // rssFeed取得
        var txt = jQuery(this).html();

        // 記事1件ごとに処理
        var items = txt.split('<br><br>');
        jQuery.each(items, function(index, value) {
            if (value.trim().length > 0) {

            // タイトル、日付、リンクに分解
            var item = value.match(/(<span.*?<\/span>)[\s\S]*?(\(.*?\))[\s\S]*?<br>[\s\S]*?(<a href=.*?\>[\s\S]*?<\/a>)/);

            // タイトル
            var title = item[1];

            // 日付
            var rdate = item[2];
            jQuery.each(week,function(key,value){
                rdate = rdate.replace(value,key)
            });
            jQuery.each(month,function(key,value){
                rdate = rdate.replace(value,key)
            });
            rdate = rdate.replace(/\(([日月火水木金土]).*?(\d+).*?(\d+).*?(\d+)\)/,
                    '$4年 $3月 $2日 \($1\)<br>');

            // リンク
            var link = item[3].replace('target="_blank"','target="_self"');

            // 修正内容で書き戻す
            items[index]= title + rdate + link;
            }
        });

        // rssFeed修正
        jQuery(this).html( items.join('<br><br>') );
    });
});

